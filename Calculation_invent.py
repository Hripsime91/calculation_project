import numpy as np
import pandas as pd

x = np.linspace(18.64, 20.03, 60)
y = np.linspace(0.59979, 0.61892, 60)
dict_of_values = {"TAV": x, "Coefficient": y}
calc = pd.DataFrame(dict_of_values)
print(calc)

tav_to_calculate = float(input("Input Number: "))

matching_rows = calc[np.isclose(calc["TAV"], tav_to_calculate, atol=1e-3, rtol=1e-3)]

if not matching_rows.empty:
    results = tav_to_calculate * matching_rows["Coefficient"]
    print("Results:")
    for index, result in results.items():
        print(f"Row {index}: Result: {result}")
else:
    print("TAV value not found in the DataFrame.")

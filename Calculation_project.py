from PyPDF2 import PdfReader

reader = PdfReader("volsub.pdf")
page = reader.pages[1]
check_list = (
    "Acetaldehyde",
    "Acrolein",
    "Ethylacetate",
    "Acetal",
    "Methanol",
    "?",
    "Ethylbutyrate",
    "Butanol-2",
    "Propanol-1",
    "Isoamylacetate",
    "Methyl-2-propanol-1",
    "Allilicalcohole",
    "Butanol-1",
    "Methyl-4-pentanol-2",
    "Methyl-2-butanol",
    "Methyl-3-butanol",
    "Ethylcaproate",
    "Ethyllactate",
    "Hexanol-1",
    "Ethylcaprylate",
    "Ethylcaprate",
    "Ethyllaurate",
    "Heranyol",
    "Phenyl-2-ethanol",
)
parts = []


def visitor_body(text, cm, tm, fontDict, fontSize):
    y = tm[5]
    if y > 350 and y < 680:
        parts.append(text)


page.extract_text(visitor_text=visitor_body)
text_body = "".join(parts)

# print(text_body)
tav = float("40.1")
splited = []
values = []


class Calculation:
    def __init__(self):
        print()

    for line in parts:
        a = line.split()
        if a == []:
            continue
        else:
            splited.append(a)

    def checking(self):
        i = 0
        for _ in splited:
            if splited[i][-1] == check_list[i]:
                pass
            else:
                print(f"{splited[i][-1]} != {check_list[i]}")
                return False
            i += 1
        self.list_of_values()

    def list_of_values(self):
        for line in splited:
            v = line[-2]
            if v == "-":
                v = 0
            values.append(v)
        # print(values)
        self.calc()

    def calc(self):
        acetadehydes = round(
            ((float(values[3]) * 0.3729 + float(values[0])) * 100) / tav, 1
        )
        esters = round(((float(values[17]) * 0.7454 + float(values[2])) * 100) / tav, 1)
        higher_alcohols = round(
            (
                (
                    float(values[7])
                    + float(values[8])
                    + float(values[10])
                    + float(values[12])
                    + float(values[14])
                    + float(values[15])
                )
                * 100
            )
            / tav,
            1,
        )
        methanol = round((float(values[4]) * 0.1) / tav, 1)
        print(f"Acetaldehydes__ {acetadehydes}")
        print(f"Esters__ {esters}")
        print(f"Higher alcohols__ {higher_alcohols}")
        print(f"Methanol__ {methanol}")


my_calculation = Calculation()

my_calculation.checking()
# if checking() != False:
#     checking()
